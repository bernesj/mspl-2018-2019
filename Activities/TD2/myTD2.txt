----------------------- Film Genre Popularity 1910-2018 ----------------------- 

//Data scrapped from IMDB database.

//Movies genres: action, thriller, war, sci-fi, romance, crime, musicals, comedy, horror, documentary,
western, fantasy

Obervations :

Except for the comedy genre's popularity that stayed stable (and is by far the most popular genre)
since the 50s, the movies genres popularity evoluted a lot since 1900.
There's a huge increase of popularity of the horror genre since the 50s, as well as 
the documentary genre since the 90s.
Western genre however, has complety disapeared over the past 40 years.

The thriller genre�s graph is just like the thriller genre itself: slowly building intensity over time.
 